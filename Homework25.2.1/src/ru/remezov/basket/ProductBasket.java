package ru.remezov.basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductBasket implements Basket{
    Map<Integer, Product> productMap = new HashMap<>();
    Integer article = 0;

    @Override
    public void addProduct(String product, int quantity) {
        for (Map.Entry<Integer, Product> productEntry: productMap.entrySet()) {
            if (productEntry.getValue().getProduct() == product) {
                System.out.printf("A product with that name %s is created.\n", product);
                return;
            }
        }
        productMap.put(article, new Product(product, quantity));
        article++;
    }

    @Override
    public void removeProduct(String product) {
        for (Map.Entry<Integer, Product> productEntry: productMap.entrySet()) {
            if (productEntry.getValue().getProduct() == product) {
                productMap.remove(productEntry.getKey());
                return;
            }
        }
        System.out.println("This product is not found.");
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Map.Entry<Integer, Product> productEntry: productMap.entrySet()) {
            if (productEntry.getValue().getProduct() == product) {
                productEntry.getValue().setQuantity(quantity);
                return;
            }
        }
        System.out.println("This product is not found");
    }

    @Override
    public void clear() {
        if (productMap.isEmpty()) {
            System.out.println("The basket is empty.");
            return;
        }

        productMap.clear();
        System.out.println("The basket is cleared.");
    }

    @Override
    public List<String> getProducts() {
        if (productMap.isEmpty()) {
            System.out.println("The basket is empty.");
            return null;
        }

//        List<String> productName = new ArrayList<>();
//        for (Map.Entry<Integer, Product> productEntry: productMap.entrySet()) {
//            productName.add(productEntry.getValue().getProduct());
//        }
//        return productName;

        //Второй вариант
        List<Product> productList = new ArrayList<>(productMap.values());
        List<String> productName = new ArrayList<>();
        for (Product prod: productList) {
            productName.add(prod.getProduct());
        }
        return productName;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Map.Entry<Integer, Product> productEntry: productMap.entrySet()) {
            if (productEntry.getValue().getProduct() == product) {
                return productEntry.getValue().getQuantity();
            }
        }
        System.out.println("This product is not found");
        return 0;
    }
}
