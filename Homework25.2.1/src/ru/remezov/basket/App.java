package ru.remezov.basket;

public class App {
    public static void main(String[] args) {
        ProductBasket productBasket = new ProductBasket();
        productBasket.addProduct("Cola",15);
        productBasket.addProduct("Pepsi",25);
        productBasket.addProduct("Fanta",5);
        productBasket.addProduct("Mirinda",10);
        productBasket.addProduct("Cola",15);

        //productBasket.removeProduct("Cola");
        //productBasket.clear();
        //productBasket.updateProductQuantity("Pepsi", 5);
        //System.out.println(productBasket.getProductQuantity("Pepsi"));

        System.out.println(productBasket.getProducts());

        System.out.println(productBasket.productMap.keySet());
    }
}
